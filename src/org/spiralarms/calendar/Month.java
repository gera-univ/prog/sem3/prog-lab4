package org.spiralarms.calendar;

public enum Month {
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December;

    private static boolean isLeapYear(int year) {
        return (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0));
    }

    public static int getDaysInMonth(Month month, int year) {
        if (month == Month.January) return 31;
        else if (month == Month.February) return 28 + (isLeapYear(year) ? 1 : 0);
        else if (month == Month.March) return 31;
        else if (month == Month.April) return 30;
        else if (month == Month.May) return 31;
        else if (month == Month.June) return 30;
        else if (month == Month.July) return 31;
        else if (month == Month.August) return 31;
        else if (month == Month.September) return 30;
        else if (month == Month.October) return 31;
        else if (month == Month.November) return 30;
        else if (month == Month.December) return 31;
        return 0; // null
    }

}
