package org.spiralarms.calendar;

import java.util.ArrayList;

public class Calendar {
    private final Date date;
    private final DayOfWeek dayOfWeek;

    public Calendar(Date date, DayOfWeek dayOfWeek) {
        this.date = date;
        this.dayOfWeek = dayOfWeek;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(DayOfWeek.Monday + "\t");
        sb.append(DayOfWeek.Tuesday + "\t");
        sb.append(DayOfWeek.Wednesday + "\t");
        sb.append(DayOfWeek.Thursday + "\t");
        sb.append(DayOfWeek.Friday + "\t");
        sb.append(DayOfWeek.Saturday + "\t");
        sb.append(DayOfWeek.Sunday + "\t\n");

        int offset = getMondayOffset();

        for (int i = 0; i < offset; ++i) {
            sb.append("  \t");
        }

        for (int day = 1; day <= getDaysInMonth(); ++day) {
            sb.append(String.format("%02d", day));
            if (day == date.day)
                sb.append("*");
            sb.append("\t");
            if ((day + offset) % 7 == 0)
                sb.append("\n");
        }
        return sb.toString();
    }

    private int getMondayOffset() {
        return (dayOfWeek.getNumberInWeek() - date.getDay() % 7 + 7) % 7;
    }

    public Date getDate() {
        return date;
    }

    public int getDaysInMonth() {
        return Month.getDaysInMonth(date.getMonth(), date.getYear());
    }

    public DayOfWeek getDayOfWeek(int day) throws Exception {
        if (day > Month.getDaysInMonth(date.month, date.year) || day <= 0)
            throw new Exception("Incorrect day of month");
        int daysInMonth = getDaysInMonth();
        int offset = (date.getDay() + daysInMonth) % daysInMonth - day;
        return DayOfWeek.getDayOfWeek((dayOfWeek.getNumberInWeek() - offset - 1) % 7 + 1);
    }

    public ArrayList<Integer> getDays(DayOfWeek dayOfWeek) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int day = getMondayOffset() + 1; day <= getDaysInMonth(); day += 7)
            result.add(day);
        return result;
    }
}
