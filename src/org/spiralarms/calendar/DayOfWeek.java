package org.spiralarms.calendar;

public enum DayOfWeek {
    Monday(1),
    Tuesday(2),
    Wednesday(3),
    Thursday(4),
    Friday(5),
    Saturday(6),
    Sunday(7);

    private final int numberInWeek;

    DayOfWeek(int numberInWeek) {
        this.numberInWeek = numberInWeek;
    }

    public static DayOfWeek getDayOfWeek(int numberInWeek) throws Exception {
        if (numberInWeek == 1) return Monday;
        if (numberInWeek == 2) return Tuesday;
        if (numberInWeek == 3) return Wednesday;
        if (numberInWeek == 4) return Thursday;
        if (numberInWeek == 5) return Friday;
        if (numberInWeek == 6) return Saturday;
        if (numberInWeek == 7) return Sunday;
        throw new Exception("Incorrect day of week");
    }

    public int getNumberInWeek() {
        return numberInWeek;
    }

    @Override
    public String toString() {
        return super.toString().substring(0, 1);
    }
}
