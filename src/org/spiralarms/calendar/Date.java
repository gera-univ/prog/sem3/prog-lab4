package org.spiralarms.calendar;

public class Date {
    int day;
    Month month;
    int year;

    public Date(int day, Month month, int year) throws Exception {
        this.day = day;
        this.month = month;
        this.year = year;

        checkValidDate();
    }

    @Override
    public String toString() {
        return day + " of " + month + ", " + year;
    }

    private void checkValidDate() throws Exception {
        if (day > Month.getDaysInMonth(month, year) || day <= 0)
            throw new Exception("Invalid date");
    }

    public int getDay() {
        return day;
    }

    public Month getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }
}
