package org.spiralarms.calendar_test;

import org.spiralarms.calendar.Calendar;
import org.spiralarms.calendar.Date;
import org.spiralarms.calendar.DayOfWeek;
import org.spiralarms.calendar.Month;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception {
        ArrayList<Calendar> calendars = new ArrayList<>(12);
        calendars.add(new Calendar(new Date(01, Month.January, 2020), DayOfWeek.Wednesday));
        calendars.add(new Calendar(new Date(01, Month.February, 2020), DayOfWeek.Saturday));
        calendars.add(new Calendar(new Date(01, Month.March, 2020), DayOfWeek.Sunday));
        calendars.add(new Calendar(new Date(01, Month.April, 2020), DayOfWeek.Wednesday));
        calendars.add(new Calendar(new Date(01, Month.May, 2020), DayOfWeek.Friday));
        calendars.add(new Calendar(new Date(01, Month.June, 2020), DayOfWeek.Monday));
        calendars.add(new Calendar(new Date(01, Month.July, 2020), DayOfWeek.Wednesday));
        calendars.add(new Calendar(new Date(01, Month.August, 2020), DayOfWeek.Saturday));
        calendars.add(new Calendar(new Date(01, Month.September, 2020), DayOfWeek.Tuesday));
        calendars.add(new Calendar(new Date(05, Month.October, 2020), DayOfWeek.Monday));
        calendars.add(new Calendar(new Date(01, Month.November, 2020), DayOfWeek.Sunday));
        calendars.add(new Calendar(new Date(01, Month.December, 2020), DayOfWeek.Tuesday));
        for (Calendar c : calendars) {
            System.out.printf("%s\t%d\t\n%s\n\n", c.getDate().getMonth(), c.getDate().getYear(), c);
        }

        Date toLookFor = new Date(13, Month.January, 2020);
        Calendar month = calendars.get(0); // January
        System.out.printf("Day of %s is %s\n", toLookFor, month.getDayOfWeek(toLookFor.getDay()));
        DayOfWeek dayOfWeek = DayOfWeek.Friday;
        ArrayList<Integer> daysOfMonth = month.getDays(dayOfWeek);
        System.out.print("Fridays of January: ");
        for (int i = 0; i < daysOfMonth.size() - 1; ++i)
            System.out.print(daysOfMonth.get(i) + ", ");
        System.out.println(daysOfMonth.get(daysOfMonth.size() - 1));
    }
}
