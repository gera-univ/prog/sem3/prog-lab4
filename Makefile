# lab 4

.POSIX:

SRC = src
MAIN = src/org/spiralarms/calendar_test/Main.java
BUILD = build
DIST = dist
JARFILE = calendar.jar
MANIFEST = META-INF/MANIFEST.mf

all: options build

# Print build options
options:
	@echo calendar build options:
	@echo none yet

# Build source files
build:
	mkdir -p $(BUILD)
	javac -cp $(SRC) $(MAIN) -d $(BUILD)

# Create the jar archive
jar: build
	mkdir -p $(DIST)
	jar -cmvf $(MANIFEST) $(DIST)/$(JARFILE) -C $(BUILD) .

# Run the jar archive
run: jar
	java -jar $(DIST)/$(JARFILE)

# Remove the binaries
clean:
	rm -rf $(BUILD) $(DIST)
